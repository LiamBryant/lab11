#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define DEBUG

#ifdef DEBUG 
#else
#endif

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
	int arrlen =10;
	char ** diction = malloc(arrlen  * sizeof(char*));
	
	// Read the dictionary line by line.
	int entries =0;
	
	char input[40];
	while(fgets(input,40,in)!= NULL)
	{
		// Expand array if necessary (realloc).
		if(entries == arrlen)
		{
			arrlen+=10;
			diction = realloc(diction,arrlen  * sizeof(char*));
			
		}
		
		//chop off newline
		char* newln = strchr(input,'\n');
		if(!(newln == NULL)){*newln = '\0';}

		// Allocate memory for the string (str).
		char* str = malloc(strlen(input)*sizeof(char));
		
		// Copy each line into the string (use strcpy).
		strcpy(str,input);
		// Attach the string to the large array (assignment =).	
		diction[entries] = str; 
		entries++;
	}
	
	
	// The size should be the number of entries in the array.
	*size = entries;
	// Return pointer to the array of strings.
	return diction;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}